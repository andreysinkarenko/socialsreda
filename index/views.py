from django.shortcuts import render
from django.http import JsonResponse, HttpResponse
from django.contrib.auth.decorators import login_required, user_passes_test
from django.views.decorators.clickjacking import xframe_options_exempt
from django.views.decorators.csrf import csrf_exempt

from .models import Generation, Dhp, Dnd, Dsc, Ddf
from .utils import *


def index_view(request):
    adm, okr = request.GET.get('adm', ''), request.GET.get('okr', '')
    ok = list(okr_s.keys())
    ads, is_new = get_ads(okr=okr)
    ad = [adm_s_inv[i] for i in ads] if is_new else list(adm_s.keys())
    if adm not in ad:
        adm = ''
    ads = get_ads(adm, okr)[0]
    df = Ddf.objects.filter(adm_zid__in=ads).values_list('children', 'school_k')
    sc = Dsc.objects.filter(adm_zid__in=ads).count()
    school_k = [i[1] for i in df]
    indicators = [('Количество детей 6-18 лет', sum([i[0] for i in df])),
                  ('Количество школ', sc),
                  ('Средняя обеспеченность', round(sum(school_k) / len(school_k), 2))]
    school_indicator = [len(list(filter(lambda x: x > 1.1, school_k))),
                        len(list(filter(lambda x: 0.9 <= x <= 1.1, school_k))),
                        len(list(filter(lambda x: x < 0.9, school_k)))]
    return render(request, 'index.html', {'adm': adm, 'okr': okr, 'ad': ad, 'ok': ok, 'indicators': indicators,
                                          'school_indicator': school_indicator})


@xframe_options_exempt
def folium_view(request):
    adm, okr = request.GET.get('adm'), request.GET.get('okr')
    ads, is_new = get_ads(adm, okr)
    if is_new:
        return HttpResponse(get_map(ads).get_root().render())
    return render(request, 'folium.html')


def polygon_view(request):
    lat = request.GET.get('lat', '')[:10]
    lng = request.GET.get('lng', '')[:10]
    try:
        if lat and lng:
            res = {'status': True}
            res.update(get_polygon_by_cad(get_cad_by_text(f"{lat}+{lng}", '1')))
            res.update({'isochrone': get_isochrone_polygon(lat, lng)})
            return JsonResponse(res)
    except:
        pass
    return JsonResponse({'status': False})


def grasshopper_view(request):
    try:
        radius = int(request.GET.get('radius', ''))
    except:
        radius = ''
    try:
        seats = int(request.GET.get('seats', ''))
    except:
        seats = ''
    lat = request.GET.get('lat', '')[:10]
    lng = request.GET.get('lng', '')[:10]
    if not(lat and lng and radius and seats):
        return JsonResponse({'status': False})
    polygon = get_polygon_by_cad(get_cad_by_text(f"{lat}+{lng}", '1'))['polygon']
    gen = Generation(lat=lat, lng=lng, polygon=polygon, radius=radius, seats=seats)
    gen.save()
    return JsonResponse({'status': True, 'n': gen.pk})


def generation_view(_, pk):
    gen = Generation.objects.filter(pk=pk).first()
    if not(gen and gen.is_generated):
        return JsonResponse({'status': False})
    res = {'new_index': gen.new_index, 'grasshopper_img': f"/static/gens/{gen.pk}.jpg",
           'objects': json.loads(gen.schools), 'status': True}
    return JsonResponse(res)


@csrf_exempt
def grass_get_view(request):
    if request.method != 'POST' or request.POST.get('mine_key') != MINE_TOKEN:
        return JsonResponse({'status': False})
    gen = Generation.objects.filter(is_generated=False).first()
    if gen is None:
        return JsonResponse({'status': False})

    data = {'idk': gen.pk, 'lat': gen.lat, 'lng': gen.lng, 'polygon': gen.polygon,
            'radius': gen.radius, 'seats': gen.seats, 'bbox': get_bbox_osm(gen.lat, gen.lng, 800)}
    return JsonResponse({'status': True, **data})


@csrf_exempt
def grass_post_view(request):
    if request.method != 'POST' or request.POST.get('mine_key') != MINE_TOKEN:
        return JsonResponse({'status': False})
    gen = Generation.objects.filter(pk=int(request.POST.get('idk'))).first()
    if gen is None:
        return JsonResponse({'status': False})

    save_base64_to_file(request.POST.get('image'), f"{STATIC_ROOT}/gens/{gen.pk}.jpg")
    gen.new_index = round(float(request.POST.get('new_index')), 2)
    gen.schools = request.POST.get('schools')
    gen.is_generated = True
    gen.save()
    return JsonResponse({'status': True})


@login_required
@user_passes_test(lambda user: user.is_superuser)
def dfs_view(request):
    if request.method != 'POST':
        return JsonResponse({'status': False})
    for i, cls in enumerate([Dhp, Dnd, Dsc, Ddf], 1):
        cls.objects.all().delete()
        df = pd.read_csv(request.FILES[f"d{i}.csv"]).drop(columns={'Unnamed: 0'})
        bulk_create_iter(cls, df)
    m = get_map(get_ads()[0])
    m.save(str(BASE_DIR) + '/templates/folium.html')
    return JsonResponse({'status': True})
