import folium
from folium.plugins import HeatMap
from rosreestr2coord import Area
import shapely.wkt
from shapely.geometry import shape
from geopy.distance import geodesic
from decimal import Decimal
import pandas as pd
import requests
import base64
import json
import time

from .choices import *


def get_price_format(price, add_text='', with_zero=False):
    if not price and not(with_zero and price == 0):
        return '-'
    return '{:,}'.format(price).replace(',', ' ') + add_text


def get_cord_delta(lat, lng, radius=500):
    lat, lng, v = Decimal(lat), Decimal(lng), Decimal('0.01')
    func = lambda x: Decimal(str(v / Decimal(int(geodesic((lat, lng), x).m) / radius))[:8])
    return func((lat + v, lng)), func((lat, lng + v))


def get_bbox_osm(lat, lng, radius=500):
    lat = str(round(lat, 6)) if type(lat) != str else lat
    lng = str(round(lng, 6)) if type(lng) != str else lng
    lat, lng = Decimal(lat), Decimal(lng)
    lat_d, lng_d = get_cord_delta(lat, lng, radius)
    return ','.join([str(i) for i in [lng-lng_d, lat-lat_d, lng+lng_d, lat+lat_d]])


def save_base64_to_file(str_data, path):
    with open(path, 'wb') as f:
        f.write(base64.b64decode(str_data))


def get_pkk_request(url):
    for _ in range(3):
        try:
            return requests.get(url).json()
        except json.decoder.JSONDecodeError:
            time.sleep(1)
        except requests.exceptions.ConnectionError:
            time.sleep(3)
        except:
            time.sleep(1)
    return None


def get_cad_by_text(text, types=''):
    if not text:
        return ''
    add_text = f"&types=[{types}]" if types else ''
    r_json = get_pkk_request(f"https://pkk.rosreestr.ru/api/features/1?text={text}&tolerance=32{add_text}")
    if r_json is None or r_json['total'] == 0:
        return ''
    return r_json['features'][0]['attrs']['id']


def get_polygon_by_cad(cad_number):
    for _ in range(3):
        try:
            area = Area(cad_number, use_cache=False, epsilon=20, with_log=False)
            cords = area.get_coord()
            if not cords:
                time.sleep(1)
                continue
            poly = [[i[1], i[0]] for i in cords[0][0]]
            if len(poly) < 3:
                time.sleep(1)
                continue
            polygon = json.dumps(poly)
            center = area.get_attrs()['center']
            center = f"{round(center['y'], 6)},{round(center['x'], 6)}"
            return {'polygon': polygon, 'center': center}
        except:
            time.sleep(3)
    raise Exception('get_poly_and_center_by_cad: n>=3')


def get_isochrone_polygon(lat, lng):
    r = requests.get(f"https://graphhopper.com/api/1/isochrone?point={lat},{lng}&key={GRP_KEY}&profile=foot")
    # poly = shape(r.json()['polygons'][0]['geometry'])
    return r.json()['polygons'][0]['geometry']


def get_df(objects, columns):
    return pd.DataFrame(objects.values_list(*columns), columns=columns)


def get_ads(adm=None, okr=None):
    adm = adm_s.get(adm)
    okr = okr_s.get(okr)
    if adm:
        ads = [adm]
    elif okr:
        ads = okr
    else:
        ads = list(adm_s.values())
    return ads, bool(adm or okr)


def get_styles_for_geo(color, opacity=0.5):
    def func(_):
        return {'weight': 1, 'color': '#ffffff', 'fillColor': color, 'fillOpacity': opacity}
    return func


def get_map(ads):
    from .models import Dhp, Dnd, Dsc, Ddf
    hp = list(Dhp.objects.filter(adm_zid__in=ads).values_list('lat', 'lng', 'population'))
    nd = get_df(Dnd.objects.filter(adm_zid__in=ads), ['name', 'address', 'year', 'center', 'flats', 'area', 'color'])
    sc = get_df(Dsc.objects.filter(adm_zid__in=ads), ['name', 'address', 'students', 'rating', 'center', 'color'])
    df = get_df(Ddf.objects.filter(adm_zid__in=ads),
                ['adm_name', 'population', 'children', 'students', 'school_k', 'rating', 'color', 'WKT'])
    df['shapely'] = df['WKT'].apply(shapely.wkt.loads)
    nd.center = nd.center.apply(json.loads)
    sc.center = sc.center.apply(json.loads)

    lat_s, lng_s = [float(i[0]) for i in hp], [float(i[1]) for i in hp]
    m = folium.Map(location=[sum(lat_s) / len(lat_s), sum(lng_s) / len(lng_s)], zoom_start=11, tiles=None)
    m.get_root().header.add_child(folium.CssLink('/static/css/folium.css'))
    folium.TileLayer('cartodbpositron', name='Слои карты').add_to(m)
    layer_districs = folium.FeatureGroup('Обеспеченность районов').add_to(m)
    layer_new_buildings = folium.FeatureGroup('Новостройки', show=False).add_to(m)
    layer_schools = folium.FeatureGroup('Школы').add_to(m)
    HeatMap(hp, 'Плотность населения', radius=12, show=False).add_to(m)

    folium.WmsTileLayer('https://pkk.rosreestr.ru/arcgis/rest/services/PKK6/CadastreObjects/MapServer/export?',
                        'show:21', fmt='PNG32', transparent=True,
                        f='image', _ts=False, bboxSR='102100', imageSR='102100', size='1024,1024',
                        dpi=96, minZoom=14, maxNativeZoom=14, name='Кадастровый слой').add_to(m)
    folium.LayerControl().add_to(m)

    for item in nd.itertuples():
        texts = [f"<b>{item.name}</b>", f"Адрес: {item.address}", f"Сдача: {item.year}",
                 f"Квартир: {item.flats}", f"Площадь: {item.area}"]
        tooltip = folium.Tooltip('<br>'.join(texts))
        icon = folium.features.CustomIcon('https://sreda.online/static/img/works.svg', icon_size=(14, 14))
        folium.Marker(item.center[::-1], 7, tooltip=tooltip, icon=icon).add_to(layer_new_buildings)

    for item in sc.itertuples():
        texts = [f"<b>{item.name}</b>", f"Адрес: {item.address}", f"Учащихся в школе, чел.: {item.students}",
                 f"Рейтинг школы: {item.rating}"]
        tooltip = folium.Tooltip('<br>'.join(texts))
        folium.CircleMarker(item.center[::-1], 7, tooltip=tooltip, fill=True, color=item.color, fill_opacity=0.7,
                            weight=0).add_to(layer_schools)

    for item in df.itertuples():
        texts = [f"<b>{item.adm_name}</b>", f"Население, чел.{item.population}",
                 f"Детей 6-18 лет, всего: {item.children}", f"Учащихся в школах района, чел.: {item.students}",
                 f"Индекс обеспеченности: {item.school_k}", f"Рейтинг школ: {item.rating}"]
        tooltip = folium.Tooltip('<br>'.join(texts))
        folium.GeoJson(data=item.shapely, style_function=get_styles_for_geo(item.color), tooltip=tooltip
                       ).add_to(layer_districs)
    return m


def bulk_create_iter(cls, df, batch_size=2000):
    for i in range(0, df.shape[0], batch_size):
        data_dict = df.iloc[i:i+batch_size].to_dict('records')
        cls.objects.bulk_create((cls(**i) for i in data_dict), ignore_conflicts=True)
    return df.shape[0]
