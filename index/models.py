from django.db import models


class Generation(models.Model):
    created = models.DateTimeField(auto_now_add=True)
    lat = models.CharField(max_length=10)
    lng = models.CharField(max_length=10)
    polygon = models.TextField()
    radius = models.PositiveSmallIntegerField()
    seats = models.PositiveSmallIntegerField()
    is_generated = models.BooleanField(default=False)
    new_index = models.CharField(max_length=4, null=True, blank=True)
    schools = models.TextField(null=True, blank=True)

    @property
    def idk(self):
        return f"{self.radius}-{self.seats}({self.lat}-{self.lng})"

    def __str__(self):
        return f"[{self.created.strftime('%Y-%m-%d %H:%M:%S')}] {int(self.is_generated)}-{self.idk}"


class Dhp(models.Model):
    created = models.DateTimeField(auto_now_add=True)
    lat = models.CharField(max_length=10)
    lng = models.CharField(max_length=10)
    population = models.PositiveIntegerField()
    adm_zid = models.PositiveSmallIntegerField(db_index=True)

    def __str__(self):
        return f"[{self.created.strftime('%Y-%m-%d %H:%M:%S')}] {self.adm_zid} {self.lat}-{self.lng}"


class Dnd(models.Model):
    created = models.DateTimeField(auto_now_add=True)
    name = models.CharField(max_length=365)
    address = models.CharField(max_length=255)
    year = models.CharField(max_length=16)
    center = models.CharField(max_length=34)
    flats = models.PositiveIntegerField()
    area = models.PositiveIntegerField()
    color = models.CharField(max_length=7)
    adm_zid = models.PositiveSmallIntegerField(db_index=True)

    def __str__(self):
        return f"[{self.created.strftime('%Y-%m-%d %H:%M:%S')}] {self.adm_zid} {self.name}"


class Dsc(models.Model):
    created = models.DateTimeField(auto_now_add=True)
    name = models.CharField(max_length=255)
    address = models.CharField(max_length=255)
    center = models.CharField(max_length=34)
    students = models.PositiveIntegerField()
    rating = models.PositiveSmallIntegerField()
    color = models.CharField(max_length=7)
    adm_zid = models.PositiveSmallIntegerField(db_index=True)

    def __str__(self):
        return f"[{self.created.strftime('%Y-%m-%d %H:%M:%S')}] {self.adm_zid} {self.name}"


class Ddf(models.Model):
    created = models.DateTimeField(auto_now_add=True)
    adm_name = models.CharField(max_length=64)
    WKT = models.TextField()
    population = models.PositiveIntegerField()
    children = models.PositiveIntegerField()
    students = models.PositiveIntegerField()
    school_k = models.FloatField()
    rating = models.FloatField()
    color = models.CharField(max_length=7)
    adm_zid = models.PositiveSmallIntegerField(db_index=True)

    def __str__(self):
        return f"[{self.created.strftime('%Y-%m-%d %H:%M:%S')}] {self.adm_zid} {self.adm_name}"
