from django import template

from ..utils import *


register = template.Library()


@register.filter
def to_price(price):
    return get_price_format(price)
