from django.urls import path
from . import views


urlpatterns = [
    path('', views.index_view, name='index'),
    path('folium', views.folium_view, name='folium'),
    path('polygon', views.polygon_view, name='polygon'),
    path('grasshopper', views.grasshopper_view, name='grasshopper'),
    path('generation/<int:pk>', views.generation_view, name='generation'),
    path('grass_get', views.grass_get_view, name='grass_get'),
    path('grass_post', views.grass_post_view, name='grass_post'),
    path('dfs', views.dfs_view, name='dfs'),
]
