from django.contrib import admin
from .models import Generation, Dhp, Dnd, Dsc, Ddf

admin.site.register(Generation)
admin.site.register(Dhp)
admin.site.register(Dnd)
admin.site.register(Dsc)
admin.site.register(Ddf)
