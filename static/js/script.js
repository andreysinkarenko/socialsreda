

$('input.filter_input').bind("input", function() {
    inp = $(this); val = inp.val().toUpperCase(); a = inp.parent().find('a');
    for (i = 0; i < a.length; i++) {
        txt = a[i].textContent || a[i].innerText; a[i].style.display = txt.toUpperCase().indexOf(val) > -1 ? '' : 'none';
    }
});
$('.filter_list a').bind("click", function() {
    me = $(this); txt = me.hasClass('clear') ? '' : me.text(); par = me.parent();
    par.find('.filter_hidden').val(txt); me.parents('form').submit()
});

$('.my_1_slider, .my_2_slider').bind("change", function() {
    par = this.parentNode; mi = par.querySelector('.my_1_slider'); ma = par.querySelector('.my_2_slider');
    mi_val = parseInt(mi.value); ma_val = parseInt(ma.value);
    if (mi_val > ma_val) { $(ma).val(mi_val); $(mi).val(ma_val); }
    dev_script_rooms(parseInt(mi.getAttribute('id').slice(-1)));
});
$('input[type="range"]').bind("input", function() {
    par = this.parentNode; value_num = '';
    if (this.classList.contains('my_1_slider') | this.classList.contains('my_2_slider')) {
        mi = par.querySelector('.my_1_slider'); ma = par.querySelector('.my_2_slider');
        mi_val = parseInt(mi.value); ma_val = parseInt(ma.value);
        if (mi_val == ma_val) { value_num = par.querySelector('.text_value .value').textContent == this.value ? '2' : ''; }
        else { value_num = this.value == Math.max(ma_val, mi_val) ? '2' : ''; }
        min_val = Math.min(mi_val, ma_val); max_val = Math.max(mi_val, ma_val);
        left_val = (min_val - mi.getAttribute('min')) / (ma.getAttribute('max') - mi.getAttribute('min')) * 100;
        width_val = (max_val - min_val) / (ma.getAttribute('max') - mi.getAttribute('min')) * 100;
        range_dif = par.querySelector('.range_dif'); range_dif.style.left = left_val+'%'; range_dif.style.width = width_val+'%';
    } else {
        val = (this.value - this.getAttribute('min')) / (this.getAttribute('max') - this.getAttribute('min')) * 100;
        this.style.background = "linear-gradient(to right, #ec0e4c "+val+"%, #ddd "+val+"%)";
    }
    par.querySelector('.text_value .value'+value_num).textContent = this.value;
});
$('input[type="range"]').trigger('input');

